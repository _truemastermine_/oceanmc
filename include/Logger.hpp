#pragma once
#include "spdlog/spdlog.h"

#define OMC_LOGGER_CONSOLE "OceanMC"
#define OMC_LOGGER OceanMC::Logger::Log()

namespace OceanMC {
namespace Logger {

static inline std::shared_ptr<spdlog::logger> Log() {
    return spdlog::get(OMC_LOGGER_CONSOLE);
}

}  // namespace Logger

}  // namespace OceanMC
