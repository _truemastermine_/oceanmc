#pragma once
#include <fstream>

#include "Logger.hpp"
#define EULAFile "eula.txt"
#define EULAText \
    "#By changing the setting below to TRUE you are indicating your agreement to our EULA(https// account.mojang.com/documents/minecraft_eula).\n\
#Tue Aug 25 00 : 29 : 38 CST 2015\n\
eula = false\n"
#define EULAWarning \
    "By running this software, You are agreeing to the Mojang EULA. (https://account.mojang.com/documents/minecraft_eula)\n\
If you do not agree to the EULA, please terminate by pressing Ctl + C."

inline void validateEULA() {
    std::fstream eula(EULAFile);
    if (!eula.is_open()) {
        eula.open(EULAFile, std::ios::out);  //  DON'T FORGET READ FLAG
        eula << EULAText;
    }
    const bool agree = true;  //	 TODO: Check EULA = true
    if (!agree) {
        OMC_LOGGER->error(
            "Please agree to the eula! More info can be found in eula.txt.");
        eula.close();
        exit(1);
    }
    eula.close();
    OMC_LOGGER->warn(EULAWarning);
}
