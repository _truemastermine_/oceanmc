#pragma once

#include <unistd.h>

#include <cstring>
#include <string_view>

#include "Logger.hpp"

namespace OceanMC {
namespace Server {

struct PacketIn;
struct PacketOut;

struct Connection {
    int _sock;
    int _pVert;
    bool _open = true;

    Connection(int sock);
    Connection() = delete;
    Connection(Connection&) = delete;

    PacketOut preparePacket(int size);
    PacketIn getPacket();

    void sendPacket(PacketOut& op) const;

    inline void closeConn() {
        if (_open) {
            _open = false;
            close(_sock);
        }
    }
};

struct PacketIn {
    // const Connection* _conn;
    char* _buff;
    char* _pos;
    int _size;
    int _proNum;

    PacketIn() = delete;
    PacketIn(PacketIn&) = delete;
    // inline PacketIn(int sock) : sock(sock) {}
    PacketIn(const Connection& c);
    ~PacketIn();

    char& readByte();

    long long varNum();
    // template <class T>
    // T varNum() {
    //     T val = 0;
    //     int pos = 0;
    //     unsigned char byte;
    //
    //     while (true) {
    //         read(_conn->_sock, &byte, 1);
    //         val |= (byte & 0x7F) << pos;
    //
    //         if ((byte & 0x80) == 0) break;
    //
    //         pos += 7;
    //         if (pos >= sizeof(T) * 8) {
    //             return -1;
    //         }
    //     }
    //     return val;
    // }

    std::string_view varString();

    template <class T>
    T as() {
        T val;
        val = *(T*)_pos;
        _pos += sizeof(T);
        return val;
    }
};

struct PacketOut {
    char* _buff;
    char* _pos;

    PacketOut() : PacketOut(1024) {}
    PacketOut(int size) : _buff(new char[size]), _pos(_buff) {}
    PacketOut(PacketOut&) = delete;
    ~PacketOut() { delete[] _buff; }

    template <class T>
    void varNum(T val) {
        while (true) {
            if ((val & 0x80) == 0) {
                *_pos = val;
                _pos++;
                return;
            }

            *_pos = (val & 0x7f) | 0x80;
            _pos++;
            val >>= 7;
        }
    }

    void varString(std::string_view v) {
        varNum(v.length());
        std::memcpy(_pos, v.begin(), v.length());
        _pos += v.length();
    }

    template <class T>
    void as(T val) {
        T* t = (T*)_pos;
        *t = val;
        _pos += sizeof(T);
    }

    template <class T>
    void as(T val, int leng) {  // TODO: For Arrays
        T* t = (T*)_pos;
        *t = val;
        _pos += sizeof(T);
    }
};

}  // namespace Server

}  // namespace OceanMC
