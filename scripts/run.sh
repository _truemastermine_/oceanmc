#!/bin/sh

[[ $1 == "" ]] && cfg="debug" || cfg=$1

sh ./build.sh $cfg

cd ../build/bin/${cfg^}/ && ./OceanMC
