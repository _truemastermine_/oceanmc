#!/bin/sh

sh ./bmake.sh

[[ $1 == "" ]] && cfg="debug" || cfg=$1

cd ../build/make && make config=$cfg
