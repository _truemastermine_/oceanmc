#include "connections/Connection.hpp"

void pingRequest(const OceanMC::Server::Connection&);

namespace OceanMC {
namespace Server {

// Connection

Connection::Connection(int sock) : _sock(sock) {
    PacketIn pi(*this);
    _pVert = pi.varNum();
    pi.varString();
    pi.as<unsigned short>();
    auto mode = pi.varNum();

    if (mode == 1) {
        pingRequest(*this);
        closeConn();
    }
}

void Connection::sendPacket(PacketOut& op) const {  // todo: MAKE BETTER
    int leng = op._pos - op._buff;

    char buf[8];
    int len = 0;
    int val = leng;
    while (true) {
        if ((val & 0x80) == 0) {
            *(buf + len) = val;
            len++;
            break;
        }

        OMC_LOGGER->debug(":P {}", val);
        *(buf + len) = (val & 0x7f) | 0x80;
        len++;
        val >>= 7;
    }

    char* b = new char[len + leng];
    memcpy(b, buf, len);
    memcpy(b + len, op._buff, leng);
    write(_sock, b, len + leng);
}

// ---------
// Packet In
// ---------

PacketIn::PacketIn(const Connection& c) {
    char b;
    int pos = 0;
    do {
        read(c._sock, &b, 1);
        _size |= (b & 0x7f) << pos;
        pos += 7;
    } while ((b & 0x80) != 0);

    _buff = new char[_size];
    read(c._sock, _buff, _size);
    _pos = _buff;

    _proNum = varNum();
}

PacketIn::~PacketIn() { delete[] _buff; }

long long PacketIn::varNum() {
    long long val = 0;
    int pos = 0;
    unsigned char byte;

    while (true) {
        byte = as<unsigned char>();
        val |= (byte & 0x7F) << pos;

        if ((byte & 0x80) == 0) break;

        pos += 7;
        // if (pos >= sizeof(long long) * 8) {
        //     return -1;
        // }
    }
    return val;
}

std::string_view PacketIn::varString() {
    int length = varNum();
    char* str = new char[length];

    memcpy(str, _pos, length);
    return std::string_view(str, length);
}

}  // namespace Server

}  // namespace OceanMC

void pingRequest(const OceanMC::Server::Connection& c) {
    OceanMC::Server::PacketIn __(c);

    OceanMC::Server::PacketOut po(1024);
    po.varNum(0x00);
    po.varString(
        R"({"version": {"name": "1.19","protocol": 760 }, "players": {"max":5, "online": 1}, "description": {"text": "Hello, World!"}})");
    c.sendPacket(po);

    OceanMC::Server::PacketIn pi(c);
    auto in = pi.varNum();

    OceanMC::Server::PacketOut out(1024);
    out.varNum(0x01);
    out.varNum(in);
    c.sendPacket(out);
}