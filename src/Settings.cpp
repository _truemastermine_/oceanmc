#include "settings/Settings.hpp"

#include <cstdlib>
#include <fstream>
#include <json.hpp>
#include <sol/sol.hpp>
#include <string>
#include <iostream>

#include "Logger.hpp"

namespace OceanMC {
namespace Settings {

static sol::state* l;
static sol::table _conf;
static char* fileName = "include/settings/settings.conf";

void pull() {
    std::ifstream file(fileName);  // TODO:
    // nlohmann::json j;
    // j << file;

    // for (auto [key, val] : j.items()){

    // }



    std::string line;
    char* t;
    while (std::getline(file, line)) {
        unsigned int sep = line.find('=');
        std::string key = line.substr(0, sep);
        std::string val = line.substr(sep + 1);

        auto num = std::strtod(val.c_str(), &t);
        if (*t == '\0') {
            _conf[key] = num;
            continue;
        }
        if (val == "TRUE" || val == "FALSE") {
            _conf[key] = val == "TRUE";
            continue;
        }
        if (val != "nil") {
            _conf[key] = val;
        }
    }
}

// ---------------------------------------------------
// REMOVED, CONFIG SHOULD BE EDITED BY USER NOT SYSTEM. 
// ---------------------------------------------------

// void push() {
//     std::ofstream file(fileName, std::ios::out | std::ios::trunc);
//     file.seekp(0);
//     // file.truncate(0);

//     _conf.for_each([&file](sol::object key, sol::object val) {
//         sol::type valT = val.get_type();
//         if (key.get_type() != sol::type::string) return;

//         if (valT == sol::type::string)
//             file << key.as<std::string>() << '=' << val.as<std::string>()
//                  << '\n';
//         else if (valT == sol::type::number) {
//             file << key.as<std::string>() << '=' << val.as<double>() << '\n';
//         } else if (valT == sol::type::boolean) {
//             const char* v = val.as<bool>() ? "TRUE" : "FALSE";
//             file << key.as<std::string>() << '=' << v << '\n';
//         }
//     });
// }

sol::table& get() { return _conf; }

void initSettingsManager() {
    if (!l) l = new sol::state();
    _conf = l->create_table();
    pull();
}

void destroySettingsManager() {
    // push();
    delete l;
}

}  // namespace Settings
}  // namespace OceanMC
