#include <chrono>
#include <sol/sol.hpp>
#include <thread>

#include "Common.hpp"
#include "Logger.hpp"
#include "connections/Connection.hpp"
#include "spdlog/sinks/stdout_color_sinks.h"
#include "util/eula.hpp"
using namespace std::literals::chrono_literals;

#if defined(RELEASE)
#define OMC_CONFIG_LOG_LEVEL spdlog::level::err
#elif defined(STABLE)
#define OMC_CONFIG_LOG_LEVEL spdlog::level::trace
#else
#define OMC_CONFIG_LOG_LEVEL spdlog::level::trace
#endif

namespace OceanMC {
namespace Async {
void initThreads(int t);
void createJob(void(*f)());
}  // namespace Async

namespace Settings {
void initSettingsManager();
void destroySettingsManager();
sol::table& get();
}  // namespace Settings

namespace Server {
static void initServer();
static void mainLoop();
void startServer();
void closeServer();

static void initServer() {
    validateEULA();

    // Lua Interp
    // Settings
    Settings::initSettingsManager();
    // Async/Event
    Async::initThreads(5);
    // Extensions
    // Connections
    OceanMC::Server::startServer();
    // Finalize
}

static void mainLoop() {
    constexpr int tps = 20;
    constexpr int nspt = 1000000000 / tps;
    constexpr auto tickDiff = std::chrono::nanoseconds(nspt);
    unsigned long long tick = 0;
    auto lastTick = std::chrono::steady_clock::now();

    auto a = []{std::this_thread::sleep_for(3s); OMC_LOGGER->debug(":P");};
    Async::createJob(a);
    Async::createJob(a);
    Async::createJob(a);
    Async::createJob(a);
    Async::createJob(a);

    // auto audit = std::chrono::nanoseconds::zero();
    while (true) {
        const auto now = std::chrono::steady_clock::now();
        const auto delta = now - lastTick;
        if (delta < tickDiff) {
            std::this_thread::sleep_for(1ns);
            continue;
        }
        lastTick = now;
        tick++;
        (Settings::get())["tick"] = "test";
        (Settings::get())["num"] = 4;
        (Settings::get())["squid"] = true;
        Settings::get()["default.server.motd"] =
            "A Minecraft Server | Written In C++";
        // if (tick > 20) return;
        // audit += delta;
        // OMC_LOGGER->debug("tick #{}, {}", tick, audit.count());
    }
}
}  // namespace Server
}  // namespace OceanMC

int main() {
    // Init Logger
    spdlog::set_pattern("%T %^[%n] %v%$");
    auto var = spdlog::stdout_color_mt(OMC_LOGGER_CONSOLE);
    var->set_level(OMC_CONFIG_LOG_LEVEL);

    OceanMC::Server::initServer();

    OceanMC::Server::mainLoop();

    OceanMC::Server::closeServer();
    OceanMC::Settings::destroySettingsManager();
}