#include "connections/Connection.hpp"

#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <thread>

#include "Logger.hpp"

namespace OceanMC {
namespace Server {

void onConnection(int sock);

static const int PORT = 25565;  // TODO: SETTINGS
static int serverSock;
static std::thread acceptThread;

static void acceptLoop() {
    // Accept Incoming
    while (true) {
        sockaddr_in cli_addr;
        socklen_t cli_addrlen = sizeof(cli_addr);
        int nsock = accept(serverSock, (sockaddr*)&cli_addr, &cli_addrlen);
        OMC_LOGGER->debug("{}, {}", serverSock, nsock);
        if (nsock < 0) {
            OMC_LOGGER->error("Failed to accept connection!");
            return;  // TODO ERROR
        }

        onConnection(nsock);
    }
}

void startServer() {
    serverSock = socket(AF_INET, SOCK_STREAM, 0);
    if (serverSock < 0) {  // TODO ERROR
        return;
    }

    // Bind the socket
    sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    serv_addr.sin_port = htons(PORT);
    if (bind(serverSock, (sockaddr*)&serv_addr, sizeof(serv_addr)) < 0) {
        return;  // TODO ERROR
    }

    // Listen for Incoming Connections
    listen(serverSock, 5);

    // Launch Accepter in new Thread
    acceptThread = std::thread(acceptLoop);
}

void closeServer() {
    acceptThread.detach();
    close(serverSock);
}

void onConnection(int sock) {
    OceanMC::Server::Connection c = sock;
    // OceanMC::Server::PacketIn pi = c;
    // int len = pi.varNum<int>();
    // int id = pi.varNum<int>();
    // int ver = pi.varNum<int>();
    // auto addr = pi.varString();
    // auto port = pi.as<unsigned short>();
    // int mode = pi.varNum<int>();
    // OMC_LOGGER->debug("Connection received with mode: {}", mode);
    // if (mode == 1) {
        // OMC_LOGGER->debug("{}, {}", pi.varNum<int>(), pi.varNum<int>());
        // OceanMC::Server::PacketIn p = c;
        // OceanMC::Server::PacketOut po(1024);
        // po.varNum(0x00);
        // po.varString(R"({"version": {"name": "1.19","protocol": 760 }, "players": {"max":5, "online": 1}, "description": {"text": "Hello, World!"}})");
        // c.sendPacket(po);
        // pi.varNum<int>(); pi.varNum<int>();
        // long long v = pi.varNum<long long>();
        // OceanMC::Server::PacketOut pop(1024);
        // pop.varNum(0x01);
        // pop.varNum(v);
        // c.sendPacket(pop);
        // close(sock);
        OMC_LOGGER->debug("close");
    // }
    // else if (mode == 2) {
    // } else
        // close(sock);
}

}  // namespace Server
}  // namespace OceanMC