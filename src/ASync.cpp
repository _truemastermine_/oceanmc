#include <chrono>
#include <mutex>
#include <thread>
#include <queue>
using namespace std::literals::chrono_literals;

namespace OceanMC {
namespace Async {

typedef void (*F)();
struct JobLink {
    F func;
    // void* func;
    JobLink* next;
};

struct JobQueue {
    std::mutex mux;
    JobLink* currentJob;
    JobLink* endJob;

    JobLink* pop() {
        mux.lock();
        auto res = currentJob;
        if (currentJob) currentJob = res->next;
        mux.unlock();
        return res;
    }

    void push(JobLink* job) {
        if (!job) return;
        mux.lock();
        if (currentJob)
            endJob->next = job;
        else
            currentJob = job;

        endJob = job;
        mux.unlock();
    }
};

std::thread* ts;
JobQueue queue;
// JobLink* currentJob;
// JobLink* finalJob;
// static std::mutex mux;

void createJob(F func) {
    JobLink* link = new JobLink{func, nullptr};
    queue.push(link);

    // mux.lock();
    // if (finalJob) {
    //     finalJob->next = link;
    //     finalJob = link;
    // } else {
    //     currentJob = link;
    //     finalJob = link;
    // }
    // mux.unlock();
}

// F requestJob() {
//     mux.lock();
//     if (!currentJob) {
//         mux.unlock();
//         return nullptr;
//     }
//     JobLink* jl = currentJob;
//     F jb = jl->func;
//     currentJob = jl->next;
//     delete jl;
//     if (currentJob == nullptr) finalJob = nullptr;
//     mux.unlock();
//     return jb;
// }

static void th() {
    while (true) {
        JobLink* link = queue.pop();
        if (!link) {
            if (false) break;
            std::this_thread::sleep_for(5ms);
            continue;
        }
        link->func();
        delete link;
    }
}

void initThreads(int threads) {
    // ts = new ts
    for (int i = 0; i < threads; i++) {
        std::thread* t = new std::thread(th);
    }
}

}  // namespace Async
}  // namespace OceanMC
