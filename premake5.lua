workspace "OceanMC"
	location "build/make"
	architecture "x64"
	configurations { "Debug", "Trace", "Release"}

project "OceanMC"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"
	targetdir "build/bin/%{cfg.buildcfg}/"
	objdir "build/obj/%{cfg.buildcfg}/"

	includedirs { "include","lib/spdlog/include", "lib/sol2/include" }
	files { "src/**.cpp" }

	filter "system:windows"
		defines "WINDOWS"

	filter "configurations:Debug"
		defines { "DEBUG" }
		symbols "On"

	filter "configurations:Trace"
		defines { "STABLE" }
		optimize "On"

	filter "configurations:Release"
		defines { "RELEASE" }
		optimize "On"
